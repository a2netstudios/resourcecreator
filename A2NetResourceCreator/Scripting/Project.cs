namespace A2NetResourceCreator.Scripting
{
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows.Forms.VisualStyles;

    /// <summary>
    /// TODO The project.
    /// </summary>
    public class Project : List<SatelliteAssembly>
    {
        public Project(string resgen, string al, string workspace, string bin)
        {
            this.Resgen = resgen;
            this.Al = al;
            this.Workspace = workspace;
            this.Bin = bin;
        }

        /// <summary>
        /// TODO The al.
        /// </summary>
        public string Al { get; set; }

        /// <summary>
        /// TODO The resgen.
        /// </summary>
        public string Resgen { get; set; }

        /// <summary>
        /// TODO The workspace.
        /// </summary>
        public string Workspace { get; set; }

        public string Bin { get; set; }

        public const string SuffixResources = "resources";

        public const string TemporalDirectory = "temp260115";

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("@echo off");
            sb.AppendLine($@"set resgen=""{this.Resgen}""");
            sb.AppendLine($@"set al=""{this.Al}""");
            sb.AppendLine();
            sb.AppendLine($@"if not exist ""{this.Bin}"" md ""{this.Bin}""");
            sb.AppendLine();

            foreach (var satelliteAssemblyItem in this)
            {
                sb.AppendLine($":: Satellite Assembly {satelliteAssemblyItem.DllTemplate}");
                sb.AppendLine($@"if not exist ""{satelliteAssemblyItem.TemporalDirectory}"" md ""{satelliteAssemblyItem.TemporalDirectory}""");
                foreach (var cultureItem in satelliteAssemblyItem)
                {
                    sb.AppendLine($":: Culture {cultureItem.Culture}");
                    foreach (var file in cultureItem)
                    {
                        sb.AppendLine($@"%resgen% ""{this.Combine(satelliteAssemblyItem.Location, file.ToString())}"" ""{this.Combine(satelliteAssemblyItem.TemporalDirectory, file.ResourceName)}""");
                    }

                    sb.AppendLine();
                }

                sb.AppendLine();
            }

            foreach (var satelliteAssemblyItem in this)
            {
                foreach (var cultureItem in satelliteAssemblyItem)
                {
                    var temporal = cultureItem.ToString(satelliteAssemblyItem.TemporalDirectory);
                    sb.AppendLine(
                        $@"%al% /t:lib /culture:""{cultureItem.Culture}"" /out:""{Path.Combine(this.Bin, cultureItem.Culture, satelliteAssemblyItem.DllOut)}"" /template:""{satelliteAssemblyItem.DllTemplate}"" {temporal}");
                }
            }

            sb.AppendLine(@"@echo on");
            sb.AppendLine("pause");
            return sb.ToString();
        }

        private string Combine(string path1, string path2)
        {
            return Path.Combine(path1, path2);
        }
    }
}