namespace A2NetResourceCreator.Scripting
{
    /// <summary>
    /// TODO The file resource.
    /// </summary>
    public class FileResource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileResource"/> class.
        /// </summary>
        /// <param name="name">
        /// TODO The name.
        /// </param>
        /// <param name="culture">
        /// TODO The culture.
        /// </param>
        /// <param name="extension">
        /// TODO The extension.
        /// </param>
        public FileResource(string name, string culture, string extension)
        {
            this.Name = name;
            this.Culture = culture;
            this.Extension = extension;
        }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the extension.
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{this.Name}.{this.Culture}.{this.Extension}";
        }

        public string ResourceName => $"{this.Name}.{this.Culture}.{Project.SuffixResources}";

    }
}