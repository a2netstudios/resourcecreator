﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Class1.cs" company="">
//   
// </copyright>
// <summary>
//   TODO The project.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace A2NetResourceCreator.Scripting
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    /// <summary>
    /// TODO The culture data.
    /// </summary>
    public class CultureData : List<FileResource>
    {
        public CultureData(string culture)
        {
            this.Culture = culture;
        }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        public string Culture { get; }

        /// <inheritdoc />
        public string ToString(string location)
        {
            var sb = new StringBuilder();
            foreach (var item in this)
            {
                sb.Append($@"/embed:""{Path.Combine(location, item.ResourceName)}"" ");
            }

            return sb.ToString();
        }
    }
}