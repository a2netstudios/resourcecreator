namespace A2NetResourceCreator.Scripting
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO The satellite assembly.
    /// </summary>
    public class SatelliteAssembly : List<CultureData>
    {
        public SatelliteAssembly(string dllTemplateName, string location)
        {
            this.Location = location;
            this.DllTemplate = dllTemplateName;
            this.TemporalDirectory = Path.Combine(location, Project.TemporalDirectory);
        }

        /// <summary>
        /// Gets or sets the dll name.
        /// </summary>
        public string DllTemplate { get; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string Location { get; }

        /// <summary>
        /// Gets or sets the template.
        /// </summary>
        public string DllOut => $"{this.ExtractFilename(Path.GetFileName(this.DllTemplate).Split('.'))}.{Project.SuffixResources}{Path.GetExtension(this.DllTemplate)}";

        public string TemporalDirectory { get; }

        /// <summary>
        /// TODO The separate cultures.
        /// </summary>
        /// <param name="files">
        /// TODO The files.
        /// </param>
        /// <returns>
        /// The <see cref="SatelliteAssembly"/>.
        /// </returns>
        public SatelliteAssembly SeparateCultures(params IList<FileInfo>[] files)
        {
            foreach (var filesItem in files)
            {
                foreach (var fileItem in filesItem)
                {
                    var part = this.Extract(fileItem);
                    var culture = this.FirstOrDefault(x => x.Culture.ToLowerInvariant().Equals(part.Culture.ToLowerInvariant(), StringComparison.OrdinalIgnoreCase));
                    if (culture == null)
                    {
                        culture = new CultureData(part.Culture) { part };
                        this.Add(culture);
                    }
                    else
                    {
                        culture.Add(part);
                    }
                }
            }

            return this;
        }

        /// <summary>
        /// TODO The extract.
        /// </summary>
        /// <param name="fi">
        /// TODO The fi.
        /// </param>
        /// <returns>
        /// The <see cref="FileResource"/>.
        /// </returns>
        private FileResource Extract(FileSystemInfo fi)
        {
            var file = fi.Name.Split('.');
            var filename = this.ExtractFilename(file);
            var culture = file[file.Length - 2];
            var extension = file[file.Length - 1];
            return new FileResource(filename, culture, extension);
        }

        /// <summary>
        /// TODO The extract filename.
        /// </summary>
        /// <param name="parts">
        /// TODO The parts.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ExtractFilename(IReadOnlyList<string> parts)
        {
            if (parts.Count == 1 || parts.Count == 2)
            {
                return parts[0];
            }

            var sb = new StringBuilder();
            for (var i = 0; i < parts.Count - 2; i++)
            {
                sb.Append(parts[i] + ".");
            }

            var value = sb.ToString();
            return !value.EndsWith(".") ? value : value.Substring(0, value.Length - 1);
        }
    }
}