namespace A2NetResourceCreator.Scripting
{
    using System.Collections.Generic;

    public class LocationAssembly : List<string>
    {
        public LocationAssembly(string dllTemplate)
        {
            this.DllTemplate = dllTemplate;
        }

        public string DllTemplate { get; set; }
    }
}