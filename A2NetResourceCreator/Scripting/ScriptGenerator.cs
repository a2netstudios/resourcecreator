﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScriptGenerator.cs" company="">
//   
// </copyright>
// <summary>
//   TODO The script generator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace A2NetResourceCreator.Scripting
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// TODO The script generator.
    /// </summary>
    public class ScriptGenerator
    {
        /// <summary>
        /// TODO The al.
        /// </summary>
        private readonly string al;

        /// <summary>
        /// TODO The bin.
        /// </summary>
        private readonly string bin;

        /// <summary>
        /// TODO The resgen.
        /// </summary>
        private readonly string resgen;

        /// <summary>
        /// TODO The workspace.
        /// </summary>
        private readonly string workspace;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScriptGenerator"/> class.
        /// </summary>
        /// <param name="resgen">
        /// TODO The resgen.
        /// </param>
        /// <param name="al">
        /// TODO The al.
        /// </param>
        /// <param name="workspace">
        /// TODO The workspace.
        /// </param>
        /// <param name="bin">
        /// The bin.
        /// </param>
        public ScriptGenerator(string resgen, string al, string workspace, string bin)
        {
            this.al = al;
            this.resgen = resgen;
            this.workspace = workspace;
            this.bin = Path.Combine(workspace, bin);
        }

        /// <summary>
        /// TODO The create.
        /// </summary>
        /// <param name="assemblies">
        /// TODO The assemblies.
        /// </param>
        /// <returns>
        /// The <see cref="Project"/>.
        /// </returns>
        public Project Create(IList<LocationAssembly> assemblies)
        {
            return this.Create(this.Copy(assemblies.ToList()));
        }

        /// <summary>
        /// TODO The create.
        /// </summary>
        /// <param name="satelliteAssemblies">
        /// TODO The satellite assemblies.
        /// </param>
        /// <returns>
        /// The <see cref="Project"/>.
        /// </returns>
        public Project Create(IList<SatelliteAssembly> satelliteAssemblies)
        {
            var project = new Project(this.resgen, this.al, this.workspace, this.bin);
            foreach (var locationItem in satelliteAssemblies)
            {
                var di = new DirectoryInfo(locationItem.Location);
                var fisResx = di.GetFiles("*.resx");
                var fisRes = di.GetFiles("*.restext");
                var fisTxt = di.GetFiles("*.txt");
                locationItem.SeparateCultures(fisTxt.ToList(), fisResx.ToList(), fisRes.ToList());
                project.Add(locationItem);
            }

            return project;
        }

        /// <summary>
        /// TODO The copy.
        /// </summary>
        /// <param name="assemblies">
        /// TODO The assemblies.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private IList<SatelliteAssembly> Copy(IReadOnlyList<LocationAssembly> assemblies)
        {
            var list = new List<SatelliteAssembly>();
            foreach (var assembly in assemblies)
            {
                var path = Path.Combine(this.workspace, Path.GetFileNameWithoutExtension(assembly.DllTemplate));
                list.Add(new SatelliteAssembly(assembly.DllTemplate, path));
                var directory = !Directory.Exists(path) ? Directory.CreateDirectory(path) : new DirectoryInfo(path);
                foreach (var dll in assembly)
                {
                    var directoryInfo = new DirectoryInfo(dll);
                    foreach (var file in directoryInfo.GetFiles())
                    {
                        file.CopyTo(Path.Combine(directory.FullName, file.Name), true);
                    }
                }
            }

            return list;
        }
    }
}